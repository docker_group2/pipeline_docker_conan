 #----------------------------------------
 #     Prepare flags from make generator
 #----------------------------------------

 include conanbuildinfo.mak

 CFLAGS              += $(CONAN_CFLAGS)
 CXXFLAGS            += $(CONAN_CXXFLAGS)
 CPPFLAGS            += $(addprefix -I, $(CONAN_INCLUDE_DIRS))
 CPPFLAGS            += $(addprefix -D, $(CONAN_DEFINES))
 LDFLAGS             += $(addprefix -L, $(CONAN_LIB_DIRS))
 LDLIBS              += $(addprefix -l, $(CONAN_LIBS))
 EXELINKFLAGS        += $(CONAN_EXELINKFLAGS)

 #----------------------------------------
 #     Make variables for a sample App
 #----------------------------------------

 SRCS          = md5.cpp
 OBJS          = md5.o 
 EXE_FILENAME  = md5

 #----------------------------------------
 #     Make Rules
 #----------------------------------------

 .PHONY                  :   exe
 exe                     :   $(EXE_FILENAME)

 $(EXE_FILENAME)         :   $(OBJS)
	 g++ $(OBJS) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -o $(EXE_FILENAME)

 %.o                     :   $(SRCS)
	 g++ -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@